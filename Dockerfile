FROM ruby:latest
COPY . .
RUN bundle install
CMD ["ruby", "bin/rails","server"]
EXPOSE 3000
